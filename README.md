# CTE - Compiler Construction using C++

## Task 1:
* Clone this repository
* Create a new git branch
* Write a simple lexer for C (lex.cpp)
    * Suggestion: Use this [template](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/LangImpl01.html) and try to modify it for C
* Create [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) on Gitlab
* **DONE**